#ifndef _WIFI_H_
#define _WIFI_H_

enum wifi_event {
	WIFI_EVENT_ASSOC,
	WIFI_EVENT_FAIL,
	__WIFI_EVENT_MAX
};

typedef void (*wifi_stat_cb)(enum wifi_event event);

void wifi_init_sta(wifi_stat_cb stat_cb);

#endif
