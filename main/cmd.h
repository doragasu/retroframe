#ifndef _CMD_H_
#define _CMD_H_

#include <stdbool.h>
#include <stdint.h>

bool cmd_parser_start(uint16_t port);

#endif
