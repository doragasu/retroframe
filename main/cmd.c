#include <stdint.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <lwip/sockets.h>

#include "screen.h"

#include "util.h"
#include "cmd.h"

#define PARSER_TSK_PRIO (tskIDLE_PRIORITY + 2)
#define BATCH_TSK_PRIO (tskIDLE_PRIORITY + 1)

enum task_status {
	BATCH_ST_STOPPED,
	BATCH_ST_STOPPING,
	BATCH_ST_RUNNING,
	__BATCH_ST_MAX
};

// MAP commands cannot be in a batch. All other commands can be sent in a
// single batch. Sending a batch clears the previously active one.
#define CMD_TABLE(X_MACRO) \
	X_MACRO(MAP, map) \
	X_MACRO(BATCH, batch) \
	X_MACRO(SCROLL_ABS, scroll_abs) \
	X_MACRO(SCROLL_DELTA, scroll_delta) \
	X_MACRO(BRIGHTNESS, brightness) \
	X_MACRO(WAIT, wait) \
	X_MACRO(JUMP, jump) \

#define X_AS_ENUM(uname, lname) CMD_ ## uname,
enum cmd_type {
	CMD_TABLE(X_AS_ENUM)
	__CMD_MAX
};

#define CMD_TYPE_LEN sizeof(enum cmd_type)

struct cmd_map {
	uint16_t width_px;
	uint16_t height_px;
	uint8_t data[];	// Data must be 3*width_px*height_px long
};

struct cmd_batch {
	uint32_t len;
	uint8_t data[];
};

struct cmd_scroll_abs {
	uint16_t x;
	uint16_t y;
};

struct cmd_scroll_delta {
	int16_t x;
	int16_t y;
};

struct cmd_brightness {
	uint8_t level;
	uint8_t pad[3];
};

struct cmd_wait {
	uint32_t delay_ms;
};

struct cmd_jump {
	uint32_t offset;
};

#define X_AS_UNION(uname, lname) struct cmd_ ## lname lname;

union payload {
	CMD_TABLE(X_AS_UNION)
};

struct cmd {
	enum cmd_type type;
	union payload payload;
};

struct cmd_client {
	int s;
	int cmd_num;
};

#define X_AS_CMD_NAME(uname, lname) #uname,

static const char *cmd_name[__CMD_MAX] = {
	CMD_TABLE(X_AS_CMD_NAME)
};

#define X_AS_CMD_LEN(uname, lname) sizeof(struct cmd_ ## lname),

static const uint32_t cmd_len[__CMD_MAX] = {
	CMD_TABLE(X_AS_CMD_LEN)
};

#define X_AS_CMD_PARSER_PROTO(uname, lname) \
	static bool cmd_ ## lname ## _parse(const union payload*, \
			const struct cmd_client*);

// Expand command parser prototypes
CMD_TABLE(X_AS_CMD_PARSER_PROTO);

#define X_AS_CMD_PARSER_FN(uname, lname) cmd_ ## lname ## _parse,

static bool (* const cmd_parser_fn[__CMD_MAX])(const union payload*,
		const struct cmd_client*) = {
	CMD_TABLE(X_AS_CMD_PARSER_FN)
};

struct batch_data {
	uint32_t len;
	uint32_t pos;
	char pool[];
};

static struct batch_data *bd = NULL;
static uint8_t *map = NULL;
static enum task_status batch_stat = BATCH_ST_STOPPED;
static TaskHandle_t comm_task_h;
static TaskHandle_t batch_task_h;

static void not_in_batch_log(void)
{
	LOGE("request not allowed in batches");
}

static bool recv_exact(int s, void *data, uint32_t len)
{
	uint32_t pos = 0;
	ssize_t recvd = 1; // To enter the first loop iteration

	while (recvd > 0 && pos < len) {
		recvd = recv(s, data + pos, len - pos, 0);
		if (recvd > 0) {
			pos += recvd;
		}
	}

	if (pos != len) {
		if (0 == recvd) {
			LOGI("client closed connection");
		} else if (recvd < 0) {
			LOGW("connection error");
		}
	}

	return pos != len;
}

static bool cmd_map_parse(const union payload *cmd, const struct cmd_client *c)
{
	bool err = true;
	struct screen_map m;

	if (!c) {
		not_in_batch_log();
		goto out;
	}

	m.width = cmd->map.width_px;
	m.height = cmd->map.height_px;
	const uint32_t len = 3 * m.height * m.width;

	free(map);
	map = malloc(len);
	if (!map) {
		LOGE("failed to alloc %dx%d px map", m.width, m.height);
		goto out;
	}

	err = recv_exact(c->s, map, len);
	if (err) {
		free(map);
		map = NULL;
	} else {
		m.data = map;
		screen_map_set(&m);
		err = false;
	}

out:
	return err;
}

static bool cmd_batch_parse(const union payload *cmd,
		const struct cmd_client *c)
{
	bool err = true;
	const struct cmd_batch *bcmd = &cmd->batch;

	if (!c) {
		not_in_batch_log();
		goto out;
	}

	free(bd);
	bd = malloc(sizeof(struct batch_data) + bcmd->len);
	if (!bd) {
		LOGE("failed to alloc %d bytes", bcmd->len);
		goto out;
	}

	bd->len = bcmd->len;
	bd->pos = 0;
	err = recv_exact(c->s, bd->pool, bd->len);
	if (err) {
		free(bd);
		bd = NULL;
	}

out:
	return err;
}

static bool cmd_scroll_abs_parse(const union payload *cmd,
		const struct cmd_client *c)
{
	const struct cmd_scroll_abs *scroll = &cmd->scroll_abs;

	screen_scroll(scroll->x, scroll->y);
	screen_update();

	return false;
}

static bool cmd_scroll_delta_parse(const union payload *cmd,
		const struct cmd_client *c)
{
	const struct cmd_scroll_delta *scroll = &cmd->scroll_delta;

	screen_scroll_delta(scroll->x, scroll->y);
	screen_update();

	return false;
}

static bool cmd_brightness_parse(const union payload *cmd,
		const struct cmd_client *c)
{
	screen_bright_set(cmd->brightness.level);
	screen_update();

	return false;
}

static bool cmd_wait_parse(const union payload *cmd, const struct cmd_client *c)
{
	vTaskDelayMs(cmd->wait.delay_ms);

	return false;
}

static bool cmd_jump_parse(const union payload *cmd, const struct cmd_client *c)
{
	bool err = true;
	const uint32_t offset = cmd->jump.offset;

	if (c) {
		LOGE("only allowed in batch commands");
		goto out;
	}

	if (offset >= bd->len) {
		LOGE("requested offset %d, but batch len is %d",
				offset, bd->len);
		goto out;
	}

	bd->pos = offset;
	err = false;

out:
	return err;
}

// Return current command and advance pointer to next one.
// Return null if no more commands available or error.
static struct cmd *next_cmd(void)
{
	if (!bd->len || bd->pos >= bd->len) {
		return NULL;
	}
	struct cmd *cmd = (struct cmd*)&bd->pool[bd->pos];

	if (cmd->type < 0 || cmd->type >= __CMD_MAX) {
		return NULL;
	}

	bd->pos += cmd_len[cmd->type] + sizeof(enum cmd_type);

	return cmd;
}

static void batch_tsk(void *arg)
{
	struct cmd *cmd;

	while (true) {
		switch (batch_stat) {
		case BATCH_ST_STOPPING:
			// Stop requested, tell comm task we have stopped
			xTaskNotifyGive(comm_task_h);
			// fallthrough
		case BATCH_ST_STOPPED:
			LOGI("batch processing stopped");
			// Wait until we must resume
			ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
			LOGI("batch processing resumed");
			break;
		default:
			// Running, nothing to do
			break;
		}
		cmd = next_cmd();
		if (!cmd || cmd_parser_fn[cmd->type](&cmd->payload, NULL)) {
			// Batch is incorrect/corrupted
			free(bd);
			bd = NULL;
			// Fixme: should synchronize this modification (maybe
			// using atomics is enough?)
			batch_stat = BATCH_ST_STOPPED;
			LOGI("stopping batch processing");
		}
	}
}

static bool command_get(struct cmd_client *c, struct cmd *cmd)
{
	const uint32_t type_len = sizeof(enum cmd_type);

	bool err = recv_exact(c->s, cmd, type_len);
	if (!err && (cmd->type < 0 || cmd->type >= __CMD_MAX)) {
		LOGW("unsupported command %d", cmd->type);
		err = true;
	}

	if (!err) {
		const int payload_len = cmd_len[cmd->type];
		err = recv_exact(c->s, ((char*)cmd) + type_len, payload_len);
	}

	if (!err) {
		c->cmd_num++;
		LOGI("%s", cmd_name[cmd->type]);
	}

	return err;
}

static void client_handle(int client)
{
	struct cmd_client *c = calloc(1, sizeof(struct cmd_client));
	bool done = false;
	struct cmd cmd;

	if (!c) {
		LOGE("client memory alloc failed");
		goto out;
	}

	c->s = client;
	// read the first batch of data
	while (!done) {
		done = command_get(c, &cmd);
		if (!done) {
			done = cmd_parser_fn[cmd.type](&cmd.payload, c);
		}
	}
	LOGI("clossing session, %d commands parsed", c->cmd_num);
	
	free(c);
out:
	close(client);
}

// To be called only by the parser_tsk task
static void batch_tsk_stop(void)
{
	if (!bd) {
		// No batch data, so task is not running
		return;
	}

	if (BATCH_ST_RUNNING == batch_stat) {
		batch_stat = BATCH_ST_STOPPING;
		ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
		batch_stat = BATCH_ST_STOPPED;
	} else {
		LOGW("ignoring stop request for not running task");
	}
}

// To be called only by the parser_tsk task
static void batch_tsk_resume(void)
{
	const enum task_status stat = batch_stat;
	if (!bd) {
		// Do not resume as there is no batch data available
		return;
	}

	if (BATCH_ST_RUNNING != stat) {
		batch_stat = BATCH_ST_RUNNING;
		xTaskNotifyGive(batch_task_h);
	} else {
		LOGW("resume requested but task is in stat %d", stat);
	}
}

static void parser_tsk(void *arg)
{
	int serv = (int)arg;

	while (true) {
		int client = accept(serv, NULL, NULL);
		if (client < 0) {
			LOGE("accept failed");
		} else {
			batch_tsk_stop();
			client_handle(client);
			batch_tsk_resume();
		}
	}
}

bool cmd_parser_start(uint16_t port)
{
	struct sockaddr_in saddr;
	socklen_t addrlen = sizeof(saddr);
	int serv;
	int optval = 1;

	// Create socket, set options
	if ((serv = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		LOGE("failed to create server socket");
		return 1;
	}

	if (setsockopt(serv, SOL_SOCKET, SO_REUSEADDR, &optval,
				sizeof(int)) < 0) {
		LOGE("setsockopt failed");
		goto err;
	}

	// Fill in address information
	memset((char*)&saddr, 0, sizeof(saddr));
	saddr.sin_family = AF_INET;
	saddr.sin_len = addrlen;
	saddr.sin_addr.s_addr = lwip_htonl(INADDR_ANY);
	saddr.sin_port = htons(port);

	// Bind to address
	if (bind(serv, (struct sockaddr*)&saddr, sizeof(saddr)) < -1) {
		LOGE("bind to port %d failed", port);
		goto err;
	}

	// Listen for incoming connections
	if (listen(serv, 0) < 0) {
		LOGE("listen to port %d failed", port);
		goto err;
	}
	LOGI("listening to port %d", port);
	
	BaseType_t result = xTaskCreate(parser_tsk, "cmd_parser", 4096,
			(void*)serv, PARSER_TSK_PRIO, &comm_task_h);
	if (result != pdPASS) {
		LOGE("parser task creation failed");
		goto err;
	}

	result = xTaskCreate(batch_tsk, "batch_parser", 4096, NULL,
			BATCH_TSK_PRIO, &batch_task_h);
	if (result != pdPASS) {
		LOGE("parser task creation failed");
		vTaskDelete(comm_task_h);
		goto err;
	}

	return 0;

err:
	close(serv);
	return 1;
}
