#ifndef _SCREEN_H_
#define _SCREEN_H_

#include <stdint.h>
#include <led_strip.h>

struct screen_map {
	const uint8_t *data;
	uint16_t width;
	uint16_t height;
};

void screen_init(uint_fast8_t pin, const struct screen_map *def_map);
void screen_map_set(const struct screen_map *map);
void screen_bright_set(uint_fast8_t level);
void screen_scroll(uint32_t x, uint32_t y);
void screen_scroll_delta(int32_t x, int32_t y);
void screen_update(void);
led_strip_t *screen_led_get(void);

#endif
