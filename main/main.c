#include <nvs_flash.h>
#include "gfx.h"
#include "screen.h"
#include "util.h"
#include "wifi.h"
#include "cmd.h"
#include "util.h"

#define SCREEN_PIN 25
#define CMD_PARSER_PORT 57450

static void log_cpuinfo(void)
{
	esp_chip_info_t chip_info;
	esp_chip_info(&chip_info);
	LOGI("ESP32 with %d CPU cores, WiFi%s%s, ", chip_info.cores,
			(chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
			(chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

	LOGI("silicon revision %d, ", chip_info.revision);

	LOGI("%dMB %s flash", (int)(spi_flash_get_chip_size() / (1024 * 1024)),
			(chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

}

static void screen_init_tsk(void *arg)
{
	UNUSED_PARAM(arg);
	const struct screen_map map = {
		.data = smb_block,
		.width = 16,
		.height = 16
	};

	screen_init(SCREEN_PIN, &map);
	screen_bright_set(8);
	screen_update();

	vTaskDelete(NULL);
}

static void wifi_cb(enum wifi_event event)
{
	static bool parser_running = false;

	if (WIFI_EVENT_ASSOC == event && !parser_running) {
		parser_running = true;
		cmd_parser_start(CMD_PARSER_PORT);
	}
}

void app_main(void)
{
	log_cpuinfo();
	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);

	wifi_init_sta(wifi_cb);
	// Screen initialization must be done on core 1, for the RMT interrupts
	// to be dispatched on core 1. Otherwise glitches will happen when WiFi
	// is enabled.
	xTaskCreatePinnedToCore(screen_init_tsk, "screen_init_tsk", 2048,
			NULL, 10, NULL, 1);
}
