#ifndef _UTIL_H_
#define _UTIL_H_

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>

// Map ESP_LOGE() macros to something easier to use
#define LOGE(...) ESP_LOGE(__func__, __VA_ARGS__)
#define LOGD(...) ESP_LOGD(__func__, __VA_ARGS__)
#define LOGI(...) ESP_LOGI(__func__, __VA_ARGS__)
#define LOGW(...) ESP_LOGW(__func__, __VA_ARGS__)

#define vTaskDelayMs(ms) vTaskDelay((ms) / portTICK_RATE_MS)

#define UNUSED_PARAM(arg) (void)(arg)

#endif /*_UTIL_H_*/

