#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <esp_system.h>
#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>
#include <nvs_flash.h>

#include <lwip/err.h>
#include <lwip/sys.h>
#include "util.h"
#include "wifi.h"

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

static bool first_init_done = false;
static int s_retry_num = 0;
static wifi_stat_cb cb = NULL;
static void event_unregister(void);

static void event_handler(void* arg, esp_event_base_t event_base,
		int32_t event_id, void* event_data)
{
	if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
		esp_wifi_connect();
	} else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
		if (s_retry_num < CONFIG_ESP_MAXIMUM_RETRY) {
			esp_wifi_connect();
			s_retry_num++;
			LOGI("retry to connect to the AP");
		} else {
			event_unregister();
			if (cb) {
				cb(WIFI_EVENT_FAIL);
			}
		}
		LOGI("connect to the AP fail");
	} else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
		event_unregister();
		ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
		LOGI("got ip:" IPSTR, IP2STR(&event->ip_info.ip));
		s_retry_num = 0;
		if (cb) {
			event_unregister();
			cb(WIFI_EVENT_ASSOC);
		}
	}
}

static void event_unregister(void)
{
	ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT,
				IP_EVENT_STA_GOT_IP, &event_handler));
	ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT,
				ESP_EVENT_ANY_ID, &event_handler));
}

void wifi_init_sta(wifi_stat_cb stat_cb)
{
	if (!first_init_done) {
		ESP_ERROR_CHECK(esp_netif_init());

		ESP_ERROR_CHECK(esp_event_loop_create_default());
		esp_netif_create_default_wifi_sta();

		wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
		ESP_ERROR_CHECK(esp_wifi_init(&cfg));
	}

	cb = stat_cb;

	ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT,
				ESP_EVENT_ANY_ID, &event_handler, NULL));
	ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT,
				IP_EVENT_STA_GOT_IP, &event_handler, NULL));

	wifi_config_t wifi_config = {
		.sta = {
			.ssid = CONFIG_ESP_WIFI_SSID,
			.password = CONFIG_ESP_WIFI_PASSWORD,
			.threshold.authmode = WIFI_AUTH_WPA2_PSK,
			.pmf_cfg = {
				.capable = true,
				.required = false
			},
		},
	};

	if (!first_init_done) {
		ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
		ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA,
					&wifi_config));
	}

	ESP_ERROR_CHECK(esp_wifi_start());

	LOGI("wifi_init_sta finished.");
	first_init_done = true;
}

